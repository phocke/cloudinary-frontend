import React, { useState, useEffect } from "react";
import { useHotkeys } from "react-hotkeys-hook";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Container from "react-bootstrap/Container";

import "./App.css";

import WelcomeMessage from "./WelcomeMessage";
import SearchForm from "./SearchForm";
import SearchResults from "./SearchResults";

function App() {
  const [activeElementIndex, setActiveElementIndex] = useState(0);
  const [activeElementIsOpen, setActiveElementIsOpen] = useState(false);
  const [searchQuery, setSearchQuery] = useState(() => {
    const params = new URLSearchParams(window.location.search);
    console.log(params.get("query"));
    // return params.get("query");
    return params.get("query") === undefined ? "" : params.get("query");
  });

  const [searchResults, setSearchResults] = useState({
    isInitial: true,
    isError: false,
    data: []
  });

  useEffect(() => {
    document.title =
      searchQuery === "" ? "Search movies" : `Searching for ${searchQuery}`;
  }, [searchQuery]);

  useHotkeys(
    "k",
    () => {
      setActiveElementIsOpen(false);
      setActiveElementIndex(prevCount => (prevCount > 1 ? prevCount - 1 : 0));
    },
    [activeElementIndex, searchResults]
  );

  // TODO - probably needs a custom hook to make it work
  useHotkeys(
    "j",
    () => {
      setActiveElementIsOpen(false);
      setActiveElementIndex(prevCount =>
        prevCount < searchResults.data.length
          ? prevCount + 1
          : searchResults.data.length - 1
      );
    },
    [activeElementIndex, searchResults]
  );

  useHotkeys("o", () => {
    setActiveElementIsOpen(isOpen => !isOpen);
  });

  useHotkeys(
    "/",
    (event, _) => {
      event.preventDefault();
      document.getElementById("search-movies-input").focus();
    },
    [searchQuery]
  );

  return (
    <Container>
      <WelcomeMessage />
      <Router>
        <SearchForm
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery.bind(this)}
          setSearchResults={setSearchResults.bind(this)}
        />
        <SearchResults
          searchResults={searchResults}
          activeElementIndex={activeElementIndex}
          activeElementIsOpen={activeElementIsOpen}
          setActiveElementIndex={setActiveElementIndex.bind(this)}
          setActiveElementIsOpen={setActiveElementIsOpen.bind(this)}
        />
      </Router>
    </Container>
  );
}

export default App;
