import React, { useState } from "react";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";

function AlertDismissible() {
  const [show, setShow] = useState(true);

  return (
    <>
      <Alert show={show} variant="success">
        <Alert.Heading>
          Hey cloudinary team{" "}
          <span role="img" aria-label="wave-icon">
            👋
          </span>
        </Alert.Heading>
        <p>
          Since this is a recruitement task and not a production ready app, I
          invite you to read
          <a href="https://bitbucket.org/phocke/cloudinary/src/master/README.md">
            {" "}
            the project readme file,{" "}
          </a>
          so that you can better understand some of the tradeoffs taken and
          reasoning behind them.
        </p>
        <p>
          As a fun little add-on feature I've decided to add simple keyboard
          navigation inspired by what you can find in gmail web client.
        </p>
        <p>
          <b>j</b> => move down
        </p>
        <p>
          <b>k</b> => move up
        </p>
        <p>
          <b>o</b> => expand / collapse search result
        </p>
        <p>
          <b>/</b> => search
        </p>
        <hr />
        <div className="d-flex justify-content-end">
          <Button onClick={() => setShow(false)} variant="outline-success">
            Close introduction.
          </Button>
        </div>
      </Alert>
    </>
  );
}

export default AlertDismissible;
