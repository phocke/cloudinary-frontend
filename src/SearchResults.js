import React from "react";
import ListGroup from "react-bootstrap/ListGroup";
import MovieResult from "./MovieResult";

function SearchResults({
  searchResults,
  activeElementIndex,
  activeElementIsOpen,
  setActiveElementIndex,
  setActiveElementIsOpen
}) {
  if (searchResults.isInitial) {
    return (
      <ListGroup as="ul" variant="flush">
        <ListGroup.Item as="li" variant="info">
          use search bar to find movies
        </ListGroup.Item>
      </ListGroup>
    );
  } else if (searchResults.isError || searchResults.data === undefined) {
    return (
      <ListGroup as="ul" variant="flush">
        <ListGroup.Item as="li" variant="light">
          no results found
        </ListGroup.Item>
      </ListGroup>
    );
  } else {
    return (
      <ListGroup as="ul" variant="flush">
        {searchResults.data.map(function(movie, index) {
          return (
            <MovieResult
              movie={movie}
              index={index}
              key={index}
              activeElementIndex={activeElementIndex}
              activeElementIsOpen={activeElementIsOpen}
              setActiveElementIndex={setActiveElementIndex.bind(this)}
              setActiveElementIsOpen={setActiveElementIsOpen.bind(this)}
            />
          );
        }, this)}
      </ListGroup>
    );
  }
}

export default SearchResults;
