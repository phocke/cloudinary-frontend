import React from "react";
import ListGroup from "react-bootstrap/ListGroup";

function MovieResult({
  movie,
  index,
  activeElementIndex,
  activeElementIsOpen,
  setActiveElementIndex,
  setActiveElementIsOpen
}) {
  function handleClick(e) {
    e.preventDefault();
    setActiveElementIsOpen(true);
    setActiveElementIndex(index);
  }

  return (
    <ListGroup.Item
      as="li"
      variant={activeElementIndex === index ? "primary  " : ""}
      key={index}
    >
      <h4>
        <a href="" onClick={handleClick}>
          {movie.Title}
        </a>
      </h4>
      {activeElementIndex === index && activeElementIsOpen ? (
        <>
          <p>
            <b>Title:</b> {movie.Title}
          </p>
          <p>
            <b>Year</b> {movie.Year}
          </p>
          <p>
            <b>imdbID</b>: {movie.imdbID}
          </p>
        </>
      ) : null}
    </ListGroup.Item>
  );
}

export default MovieResult;
