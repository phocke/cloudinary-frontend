import React from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";

function SearchForm({ searchQuery, setSearchQuery, setSearchResults }) {
  let history = useHistory();

  function updateSearchQuery(e) {
    const newSearchQuery = e.target.value;
    setSearchQuery(newSearchQuery);
    setParams(newSearchQuery);

    if (newSearchQuery.length > 2) {
      fetchData(newSearchQuery);
    }
  }

  function setParams(query = "") {
    const searchParams = new URLSearchParams();
    searchParams.set("query", query);
    history.push(`?${searchParams.toString()}`);
  }

  function fetchData(newSearchQuery) {
    axios
      // TODO configure API address in env
      .get("http://localhost:3001/search", {
        params: {
          query: newSearchQuery
        }
      })
      .then(function(response) {
        if (response.data.Response === "False") {
          setSearchResults({
            isError: true,
            isInitial: false,
            data: []
          });
        }
        setSearchResults({
          isError: false,
          isInitial: false,
          data: response.data.Search
        });
      })
      .catch(function(error) {
        setSearchResults({
          error: true,
          isInitial: false,
          data: []
        });
      });
  }

  return (
    <Form className="search-movies" onSubmit={e => e.preventDefault()}>
      <InputGroup>
        <InputGroup.Prepend>
          <InputGroup.Text>Search IMDB titles</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          placeholder="Search movies"
          id="search-movies-input"
          value={searchQuery || ""}
          onChange={updateSearchQuery}
        />
        <Button
          variant="primary"
          type="submit"
          style={{ borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
        >
          Submit
        </Button>
      </InputGroup>
    </Form>
  );
}

export default SearchForm;
